import { Component, OnInit } from '@angular/core';
import { TaskManagerService } from '../task-manager.service';
import Chart from 'chart.js/auto';

@Component({
  selector: 'app-metrics',
  standalone: true,
  imports: [],
  providers:[TaskManagerService],
  templateUrl: './metrics.component.html',
  styleUrl: './metrics.component.css'
})
export class MetricsComponent implements OnInit {
  private chart: any;
  public chartData: any;

  constructor(private taskManagerService: TaskManagerService) {}

  ngOnInit() {
      this.taskManagerService.getTasks().subscribe((tasks) => {
          this.processData(tasks);
          this.createChart();
      });
  }

  private processData(tasks: any[]) {
    const stages = ['Ready', 'In Progress', 'Testing', 'Done'];
      const priorities = Array.from(new Set(tasks.map((task) => task.priority)));

      const data = {
          labels: stages,
          datasets: priorities.map((priority) => ({
              label: priority,
              data: stages.map((stage) => tasks.filter((task) => task.status === stage && task.priority === priority).length),
              backgroundColor: this.getBackgroundColor(priority),
          })),
      };

      this.chartData = data;
  }

  private createChart() {
    const canvas = <HTMLCanvasElement>document.getElementById('myChart');
    const ctx = canvas?.getContext('2d');

    if (!ctx) {
        console.error('Unable to get canvas context');
        return;
    }
    
    this.chart = new Chart(ctx, {
        type: 'bar',
        data: this.chartData,
        options: {
            scales: {
                x: {
                    type: 'category', 
                    stacked: true,
                },
                y: {
                    stacked: true,
                },
            },
            responsive: true, 
        },
        
    });
}

  private getBackgroundColor(priority: string): string {
      
      switch (priority) {
          case 'High':
              return 'rgba(255, 165, 0, 0.5)';
          case 'Medium':
              return 'rgba(255, 255, 0, 0.5)';
          case 'Low':
              return 'rgba(0, 255, 0, 0.5)';
          default:
              return 'rgba(169, 169, 169, 0.5)';
      }
  }
}
