import { Component, OnInit } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { TaskGroupComponent } from '../task-group/task-group.component';
import { TaskManagerService } from '../task-manager.service';
import { Router } from '@angular/router';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-main-dashboard',
  standalone: true,
  imports: [FormsModule, TaskGroupComponent, CommonModule],
  providers: [TaskManagerService],
  templateUrl: './main-dashboard.component.html',
  styleUrl: './main-dashboard.component.css'
})
export class MainDashboardComponent implements OnInit {
  selectedOption: string = 'all';
  startDate: any;
  endDate: any;
  severity: any = 'allPriorities';
  assignee: string = '';
  searchQuery: string = '';
  tasks: any;
  showFilterSection: boolean = false;
  task_list: any;

  constructor(private taskManagerService: TaskManagerService, private router: Router) { }
  ngOnInit() {
    this.taskManagerService.getTasks().subscribe({
      next: (data) => {
        console.log("data", data);
        this.tasks = data;
        this.task_list = data;
      }
    }
    );
  }

  filterTasks() {
    this.tasks = this.task_list.filter((task: any) => {
      const statusMatch = this.selectedOption === 'all' || task.type.toLowerCase() === this.selectedOption.toLowerCase();
      const searchMatch = task.name.toLowerCase().includes(this.searchQuery.toLowerCase());
      const startDate = this.startDate ?task.startDate === this.startDate: this.task_list
      const endDate = this.endDate ?task.endDate === this.endDate: this.task_list;
      const assignee= task.assignee.toLowerCase().includes(this.assignee.toLowerCase());
      const priority = this.severity === 'allPriorities'||task.priority === this.severity;
      return statusMatch && searchMatch && startDate && endDate && assignee && priority;
    });
  }

  onOptionChange() {
    this.filterTasks();
  }

  onSearchChange() {
    this.filterTasks();
  }

  filterByStartDate() {
    this.filterTasks();
  }

  filterByEndDate() {
    this.filterTasks();
  }

  filterByAssignee() {
    this.filterTasks();
  }

  filterBySeverity() {
    this.filterTasks();
  }
  showMetrics() {
    this.router.navigate(['/metrics']);
  }

  toggleFilterSection() {
    this.showFilterSection = !this.showFilterSection;
  }
}
