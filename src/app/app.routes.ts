import { Routes } from '@angular/router';
import { MetricsComponent } from './metrics/metrics.component';
import { MainDashboardComponent } from './main-dashboard/main-dashboard.component';

export const routes: Routes = [
    {path:'', component:MainDashboardComponent},
    {path:'metrics', component: MetricsComponent}
];
