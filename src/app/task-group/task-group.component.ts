import { Component, Input } from '@angular/core';
import { TaskComponent } from '../task/task.component';

@Component({
  selector: 'app-task-group',
  standalone: true,
  imports: [TaskComponent],
  templateUrl: './task-group.component.html',
  styleUrl: './task-group.component.css'
})
export class TaskGroupComponent {
  inProgressTasks: any[] = [];
  curTask: any;
  totalTasks: { "ready": number, "inProgress": number, "testing": number, "done": number } = { "ready": 0, "inProgress": 0, "testing": 0, "done": 0 };
  @Input() tasks: any;
  constructor() { }

  ngOnInit() {
   this.countCheck();
  }

  countCheck(){
    this.totalTasks.done = this.totalTasks.inProgress = this.totalTasks.ready = this.totalTasks.testing = 0;
    this.tasks.forEach((task: any) => {
      if (task.status === 'Ready') {
        this.totalTasks.ready++;
      } else if (task.status === 'In Progress') {
        this.totalTasks.inProgress++;
      } else if (task.status === 'Testing') {
        this.totalTasks.testing++;
      } else if (task.status === 'Done') {
        this.totalTasks.done++;
      }
    })
  }

  onDragOver(event: any) {
    event.preventDefault();

  }

  onDrop(event: any, status: string) {
    const record = this.tasks.find((task: any) => task.id === this.curTask?.id);
    if (record != undefined) {
      record.status = status;
    }
    this.countCheck();
  }

  changecurTask(event: any) {
    this.curTask = event;
  }


}
