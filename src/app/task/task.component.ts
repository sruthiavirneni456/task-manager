import { ChangeDetectorRef, Component, EventEmitter, Input, Output } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TaskManagerService } from '../task-manager.service';

@Component({
  selector: 'app-task',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './task.component.html',
  styleUrl: './task.component.css'
})
export class TaskComponent {
  @Input() taskStatus: any;
  @Input() tasks: any[] = [];
  @Output() curTask: EventEmitter<any> = new EventEmitter<any>();
  constructor() { }



  onDragStart(event: any) {
    this.curTask.emit(event);
  }

}
