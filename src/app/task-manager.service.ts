import { HttpClient } from '@angular/common/http';
import { Injectable, signal } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TaskManagerService {
  private apiUrl = 'https://stage-mock.apiwiz.io/v1/tasks';
  private headers = {
    'x-tenant': 'b8e236df-4b26-49ef-9532-5e43ea0c10a4',
  };

  constructor(private http: HttpClient) {}
  
  getTasks(): Observable<any> {
    return this.http.get(this.apiUrl, { headers: this.headers });
  }
}
